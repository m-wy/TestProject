def propa():
    l=list()
    m=list()
    f=open("map.txt","r") 
    for i in range(12):     #On crée la matrice de l'image composée de 12 lignes de 12 nombres à partir de map.txt.
        s=f.readline()
        l=s.split()
        m=m+[l]
    for i in range(12):
        for j in range(12):
            m[i][j]=int(m[i][j])   #On transforme tout en valeurs numériques pour pouvoir faire les calculs.    
    f.close()
    for i in range(12):
        for j in range(12) :
            if m[i][j] == 9 :
                m[i][j] = 10     #Changement d'état des feu temporaires pour éviter la propagation excessive en un tour
    for i in range(12):
        for j in range(12):
            try:
                if m[i-1][j] >= 10 or m[i+1][j] >= 10 or m[i][j-1] >= 10 or m[i][j+1] >= 10 :
                    if m[i][j] == 2 :
                        m[i][j] = 9
                    if m[i][j] == 4 :
                        m[i][j] = 9
                    if m[i][j] == 7 :
                        m[i][j] = 9
                    if m[i][j] == 6 :
                        m[i][j] = 7

            except IndexError :
                print("Bordure !")
                
            if m[i][j] == 12 :
                m[i][j] = 1
            if m[i][j] == 10 or m[i][j] == 11 :
                m[i][j] = m[i][j] + 1


    f=open("map.txt","w") #On réécrit la nouvelle matrice, nouvel état de la propagation.
    for i in range(12) :
        for j in range(12) :
            f.write(str(m[i][j])+" ")
        f.write("\n")
    f.close()

propa()
