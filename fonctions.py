def gen():
    import random
    from random import randint
    test = 0
    while(test == 0) :
        l=list()
        m=list()
        for i in range(22):     #On crée la matrice de l'image composée de 22 lignes de 12 nombres à partir de map.txt.
            s="2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2"
            l=s.split()
            m=m+[l]
        
        #arbre doit se propager de 2 a 3
        for i in range(randint(4,9)):
            i=(random.randint(4,18))
            j=(random.randint(4,20))
            m[i][j]=6


        #pierre doit se propager de 1 a 2
        for i in range(randint(5,15)):
            i=(random.randint(0,21))
            j=(random.randint(0,21))
            m[i][j]=3

        #eau doit se propager de 1 a 2
        for i in range(randint(3,6)):
            i=(random.randint(4,21))
            j=(random.randint(4,21))
            m[i][j]=5


        #feu
        i=(random.randint(0,3))
        j=(random.randint(0,3))
        m[i][j]=9


        #habitation
        for i in range(randint(1,2)):
            i=(random.randint(17,21))
            j=(random.randint(17,21))
            m[i][j]=4

        for i in range(22):
            for j in range(22):
                try:
                    if m[i][j] == 6 :
                        m[i-1][j] = 16; m[i+1][j] = 16; m[i][j-1] = 16; m[i][j-2] = 16; m[i][j+1] = 16; m[i-1][j-1] = 16; m[i-2][j-1] = 16; m[i+1][j+1] = 16;
                    if m[i][j] == 5 :
                        m[i-1][j] = 15; m[i+1][j] = 15; m[i+1][j-1] = 15; m[i][j-1] = 15; m[i][j+1] = 15            
                except IndexError :
                    print("Bordure !")

        maison = 0
        feu = 0
        for i in range(22):
            for j in range(22):
                if m[i][j] == 16 :
                    m[i][j] = 6
                if m[i][j] == 15 :
                    m[i][j] = 5
                if m[i][j] == 9 or m[i][j] == 11 or m[i][j] == 12 or m[i][j] == 10 :
                    feu = feu + 1
                if m[i][j] == 4 :
                    maison = maison + 1
        if maison == 0 and feu == 0 :
            test = 0
        else :
            test = 1


        
            
    
    f=open("map.txt","w")
    for i in range(22):
        for j in range(22):
            f.write(str(m[i][j])+" ")
        f.write("\n")
    f.close()

def propa():
    l=list()
    m=list()
    f=open("map.txt","r") 
    for i in range(22):     #On crée la matrice de l'image composée de 12 lignes de 12 nombres à partir de map.txt.
        s=f.readline()
        l=s.split()
        m=m+[l]
    for i in range(22):
        for j in range(22):
            m[i][j]=int(m[i][j])   #On transforme tout en valeurs numériques pour pouvoir faire les calculs.    
    f.close()
    for i in range(22):
        for j in range(22) :
            if m[i][j] == 9 :
                m[i][j] = 10     #Changement d'état des feu temporaires pour éviter la propagation excessive en un tour
    for i in range(22):
        for j in range(22):
            try:
                if m[i-1][j] >= 10 or m[i+1][j] >= 10 or m[i][j-1] >= 10 or m[i][j+1] >= 10 :
                    if m[i][j] == 2 :
                        m[i][j] = 9
                    if m[i][j] == 4 :
                        m[i][j] = 9
                    if m[i][j] == 7 :
                        m[i][j] = 9
                    if m[i][j] == 6 :
                        m[i][j] = 7

            except IndexError :
                print("Bordure !")
                
            if m[i][j] == 12 :
                m[i][j] = 1
            if m[i][j] == 10 or m[i][j] == 11 :
                m[i][j] = m[i][j] + 1


    f=open("map.txt","w") #On réécrit la nouvelle matrice, nouvel état de la propagation.
    for i in range(22) :
        for j in range(22) :
            f.write(str(m[i][j])+" ")
        f.write("\n")

"""def affichage() :
    l=list()
    m=list()
    f=open("map.txt","r") 
    for i in range(12):     #On crée la matrice de l'image composée de 12 lignes de 12 nombres à partir de map.txt.
        s=f.readline()
        l=s.split()
        m=m+[l]
    for i in range(12):
        for j in range(12):
            m[i][j]=int(m[i][j])
    f.close()

    for i in range(12):
        for j in range(12):
            i = str(m[i][j])
            y = i * 32
            x = j * 32
            fenetre.blit(pygame.image.load(i+".jpg"), (x , y))

    pygame.display.flip() """
