import random

def map():
    l=list()
    m=list()
    for i in range(12):     #On crée la matrice de l'image composée de 12 lignes de 12 nombres à partir de map.txt.
        s="2 2 2 2 2 2 2 2 2 2 2 2"
        l=s.split()
        m=m+[l]
    
    #arbre doit se propager de 2 a 3
    i=(random.randint(2,7))
    j=(random.randint(2,7))
    m[i][j]=6


    #pierre doit se propager de 1 a 2
    i=(random.randint(0,11))
    i=(random.randint(0,11))
    m[i][j]=3


    #pierre 
    i=(random.randint(0,11))
    i=(random.randint(0,11))
    m[i][j]=3


    #eau doit se propager de 1 a 2
    i=(random.randint(0,11))
    i=(random.randint(0,11))
    m[i][j]=5


    #feu
    i=(random.randint(0,3))
    j=(random.randint(0,3))
    m[i][j]=9


    #habitation
    i=(random.randint(7,11))
    j=(random.randint(7,11))
    m[i][j]=4

    for i in range(12):
        for j in range(12):
            try:
                if m[i][j] == 6 :
                    m[i-1][j] = 16; m[i+1][j] = 16; m[i][j-1] = 16; m[i][j-2] = 16; m[i][j+1] = 16; m[i-1][j-1] = 16; m[i-2][j-1] = 16; m[i+1][j+1] = 16;
                if m[i][j] == 5 :
                    m[i-1][j] = 15; m[i+1][j] = 15; m[i+1][j-1] = 15; m[i][j-1] = 15; m[i][j+1] = 15            
            except IndexError :
                print("Bordure !")

    for i in range(12):
        for j in range(12):
            if m[i][j] == 16 :
                m[i][j] = 6
            if m[i][j] == 15 :
                m[i][j] = 5          
            
    
    f=open("map.txt","w")
    for i in range(12):
        for j in range(12):
            f.write(str(m[i][j])+" ")
        f.write("\n")
    f.close()

map()
