import pygame
from pygame.locals import *
from fonctions import *
from time import sleep

import os
os.environ['SDL_VIDEODRIVER']='windib'

pygame.init()
fenetre=pygame.display.set_mode((704,704))
icone=pygame.image.load("icone.png").convert_alpha()
pygame.display.set_icon(icone)
pygame.display.set_caption("FireWave")
pygame.display.flip()

cred = 0
jeu = 0
continuer = 1
while continuer :
    acceuil=pygame.image.load("menu.png")
    fenetre.blit(acceuil, (0,0))
    pygame.display.flip()

    for event in pygame.event.get():
        if event.type == QUIT:
            continuer = 0
            jeu = 0

        elif event.type == KEYDOWN:
            if event.key == K_SPACE:
                acceuil = 0
                jeu = 1           

            elif event.key == K_F1 :
                acceuil = 0
                cred = 1

        while jeu == 1:
            gen()
            l=list()
            m=list()
            f=open("map.txt","r") 
            for i in range(12): 
                s=f.readline()
                l=s.split()
                m=m+[l]
            for i in range(12):
                for j in range(12):
                    m[i][j]=int(m[i][j])
            f.close()

            for i in range(12):
                for j in range(12):
                    img = str(m[i][j])
                    y = i * 32
                    x = j * 32
                    fenetre.blit(pygame.image.load(img+".jpg"), (x , y))

            pygame.display.flip()
            while (fin != 1):
                propa()
                l=list()
                m=list()
                f=open("map.txt","r") 
                for i in range(12): 
                    s=f.readline()
                    l=s.split()
                    m=m+[l]
                for i in range(12):
                    for j in range(12):
                        m[i][j]=int(m[i][j])
                f.close()

                for i in range(12):
                    for j in range(12):
                        i = str(m[i][j])
                        y = i * 32
                        x = j * 32
                        fenetre.blit(pygame.image.load(i+".jpg"), (x , y))

                pygame.display.flip()
            jeu = 0

        while cred == 1 :
            cred = pygame.image.load("credits.png")
            fenetre.blit(cred, (0,0))
            pygame.display.flip()
            sleep(20)

pygame.quit()
