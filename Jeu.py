import pygame
from pygame.locals import *
from fonctions import *
from time import sleep

import os
os.environ['SDL_VIDEODRIVER']='windib'

pygame.init()
fenetre=pygame.display.set_mode((704,704))
icone=pygame.image.load("icone.png").convert_alpha()
pygame.display.set_icon(icone)
pygame.display.set_caption("FireWave")
pygame.display.flip()

cred = 0
jeu = 0
continuer = 1
while continuer :
    acceuil=pygame.image.load("menu.png")
    fenetre.blit(acceuil, (0,0))
    pygame.display.flip()

    for event in pygame.event.get():
        if event.type == QUIT:
            continuer = 0
            jeu = 0

        elif event.type == KEYDOWN:
            if event.key == K_SPACE:
                jeu = 1           

            elif event.key == K_F1 :
                cred = 1

        while jeu == 1:
            fin = 0
            gen()
            l=list()
            m=list()
            f=open("map.txt","r") 
            for i in range(22): 
                s=f.readline()
                l=s.split()
                m=m+[l]
            for i in range(22):
                for j in range(22):
                    m[i][j]=int(m[i][j])
            f.close()

            for i in range(22):
                for j in range(22):
                    img = str(m[i][j])
                    y = i * 32
                    x = j * 32
                    fenetre.blit(pygame.image.load(img+".jpg"), (x , y))
            pygame.display.flip()
            sleep(1)
            while (fin != 1):
                propa()
                l=list()
                m=list()
                f=open("map.txt","r") 
                for i in range(22): 
                    s=f.readline()
                    l=s.split()
                    m=m+[l]
                f.close()

                for i in range(22):
                    for j in range(22):
                        img = str(m[i][j])
                        y = i * 32
                        x = j * 32
                        fenetre.blit(pygame.image.load(img+".jpg"), (x , y))
                        pygame.display.flip()
                sleep(0.5)
                feu = 0
                maison = 0
                for i in range(22):
                    for j in range(22):
                        m[i][j] = int(m[i][j])
                        if m[i][j] == 9 or m[i][j] == 11 or m[i][j] == 12 or m[i][j] == 10 :
                            feu = feu + 1
                        if m[i][j] == 4 :
                            maison = maison + 1
                if feu == 0 or maison == 0 :
                    fin = 1 
                if maison == 0 :
                    gameover=pygame.image.load("gameover.png")
                    fenetre.blit(gameover, (0,0))
                    pygame.display.flip()
                    #son.play("gameover.wav")
                    sleep(10)

                if feu == 0 :
                    youwin=pygame.image.load("youwin.png")
                    fenetre.blit(youwin, (0,0))
                    pygame.display.flip()
                    #son.play("win.wav")
                    sleep(10)
                        

                pygame.display.flip()
            jeu = 0

        while cred == 1 :
            cred = pygame.image.load("credits.png")
            fenetre.blit(cred, (0,0))
            pygame.display.flip()
            sleep(20)

pygame.quit()
